package com.test.brick.io.webscapping;

import com.opencsv.CSVWriter;
import java.io.File;
import java.io.FileWriter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebScappingApplication {

  public static void main(String[] args) {

    String url = "https://www.tokopedia.com/p/handphone-tablet/handphone?ob=5&page=";
    File file = new File("web-scrapping-result.csv");
    try {
      FileWriter outputfile = new FileWriter(file);
      CSVWriter writer = new CSVWriter(outputfile);
      // adding header to csv
      String[] header = {"Name of Product", "Description", "Image Link", "Price", "Ratting",
          "Name of Store or Merchant"};
      writer.writeNext(header);
      Integer page = 1;
      for (int i = 0; i < 15; i++) {
        Document document = Jsoup.connect(url + page.toString()).get();

        Elements productList = document.select("div.css-bk6tzz");
        for (Element element : productList.select("div.css-bk6tzz")) {
          String nameOfProduct = element.select("div.css-1c0vu8l img").attr("alt");
          String description = element.select("span.css-1bjwylw").text();
          String imageLink = element.select("div.css-1c0vu8l img").attr("src");
          String price = element.select("span.css-o5uqvq").text();
          Integer ratting = element.select("div.css-153qjw7").text() != null ? (element.select("div.css-153qjw7").text().length() - 1) : 0;
          String nameOfStore = element.select("span.css-1kr22w3").last().text();

          String[] data = {nameOfProduct, description, imageLink, price, ratting.toString(),
              nameOfStore};
          // add data to csv
          writer.writeNext(data);
        }

        page++;
      }

      // closing writer connection
      writer.close();
    } catch (Exception e) {
      e.printStackTrace();
    }

  }
}

